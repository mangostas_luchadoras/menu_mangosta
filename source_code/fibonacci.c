#include <stdio.h>
#include <stdlib.h>

int fibonacci(int);

int main ()
{
    int n;

    printf("Calcula el enecimo numero de Fibonacci.\n");
    printf("\n");
    printf("Ingrese el numero a carcular...\t");

    scanf("%d", &n);
    
    printf("\n");
    printf("%d es el %dº numero de Fibonacci\n", fibonacci(n), n);

    return 0;
}

int fibonacci(int n)
{
    if (n == 0)
        return 1;
    if (n == 1)
        return 1;
    return fibonacci(n - 1) + fibonacci(n - 2);
}
