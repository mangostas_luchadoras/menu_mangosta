#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
    int n = 3;
    for(int i=0; i<n;i++)
    {
       pid_t pid;

       pid = fork();

       wait(NULL);

       printf("Soy pid: %d ,  Hijo de pid: %d\n Mi i vale: %d\n\n", getpid(), getppid(), i);
    }
    return 0;
}
