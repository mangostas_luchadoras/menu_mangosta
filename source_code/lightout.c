#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <termios.h>

#define TABLERO 9
#define FILA 3

void cambiarVecinos (int);
int posicion(int);
void vecinosEsquinas(int);
void vecinosPrimeraFila(int);
void vecinosUltimaFila(int);
void vecinosPrimeraColumna(int);
void vecinosUltimaColumna(int);
void vecinosMedio(int);

struct  tablero
{
	int casillas[TABLERO];
	int lucesApagadas;
};

struct tablero Casilleros;


int cambiarLuz(int n)
{
	Casilleros.casillas[n]= Casilleros.casillas[n]==0 ? 1 : 0;
	return Casilleros.casillas[n]==0 ? 1 : -1;
}

void cambiarCasilla(int n)
{
	Casilleros.lucesApagadas+= cambiarLuz(n);
	cambiarVecinos(n);
}

void cambiarVecinos (int n)
{
	switch(posicion(n))
	{
		case 0: vecinosEsquinas(n); break;
		case 1: vecinosPrimeraFila(n); break;
		case 2:	vecinosUltimaFila(n); break;
		case 3:	vecinosPrimeraColumna(n); break;
		case 4: vecinosUltimaColumna(n); break;
		case 5:	vecinosMedio(n); break;
	}
}

int posicion(int n)
{
	if (n == 0 || n == FILA -1 || n == TABLERO -FILA || n == TABLERO -1)
		return 0;
	if (n > 0 && n < FILA -1)
		return 1;
	if (n > TABLERO - FILA && n < TABLERO -1)
		return 2;
	if (n%FILA == 0)
		return 3;
	if (n%FILA == FILA -1)
		return 4;
	else
		return 5;
}

void vecinosEsquinas(int n)
{
	if (n == 0)
	{
		Casilleros.lucesApagadas+= cambiarLuz(1);
		Casilleros.lucesApagadas+= cambiarLuz(FILA);
	}
	if (n == FILA -1)
	{
		Casilleros.lucesApagadas+= cambiarLuz(n -1);
		Casilleros.lucesApagadas+= cambiarLuz(n +FILA);
	}
	if (n == TABLERO- FILA)
	{
		Casilleros.lucesApagadas+= cambiarLuz(n +1);
		Casilleros.lucesApagadas+= cambiarLuz(n -FILA);
	}
	if (n == TABLERO -1)
	{
		Casilleros.lucesApagadas+= cambiarLuz(n -1);
		Casilleros.lucesApagadas+= cambiarLuz(n -FILA);
	}
}

void vecinosPrimeraFila(int n)
{
	Casilleros.lucesApagadas+= cambiarLuz(n -1);
	Casilleros.lucesApagadas+= cambiarLuz(n +1);
	Casilleros.lucesApagadas+= cambiarLuz(n +FILA);
}

void vecinosUltimaFila(int n)
{
	Casilleros.lucesApagadas+= cambiarLuz(n -FILA);
	Casilleros.lucesApagadas+= cambiarLuz(n -1);
	Casilleros.lucesApagadas+= cambiarLuz(n +1);
}

void vecinosPrimeraColumna(int n)
{
	Casilleros.lucesApagadas+= cambiarLuz(n -FILA);
	Casilleros.lucesApagadas+= cambiarLuz(n +1);
	Casilleros.lucesApagadas+= cambiarLuz(n +FILA);
}

void vecinosUltimaColumna(int n)
{
	Casilleros.lucesApagadas+= cambiarLuz(n -FILA);
	Casilleros.lucesApagadas+= cambiarLuz(n -1);
	Casilleros.lucesApagadas+= cambiarLuz(n +FILA);
}

void vecinosMedio(int n)
{
	Casilleros.lucesApagadas+= cambiarLuz(n -FILA);
	Casilleros.lucesApagadas+= cambiarLuz(n -1);
	Casilleros.lucesApagadas+= cambiarLuz(n +1);
	Casilleros.lucesApagadas+= cambiarLuz(n +FILA);
}


int tableroResuelto()
{
	return Casilleros.lucesApagadas==TABLERO ? 1 : 0;
}

void llenarTablero()
{
	srand(time(NULL));
	do
	{
		for(int i = 0; i < TABLERO; i++)
		{
			Casilleros.casillas[i] = rand() % 2;
			if (Casilleros.casillas[i] == 0)
				Casilleros.lucesApagadas++;
		}
	} while(tableroResuelto());
}

void imprimirComandos()
{
    printf("#===========================================================#\n"
           "#      Presione 'a' para moverse hacia la izquierda         #\n"
           "#       Presione 'd' para moverse hacia la derecha          #\n"
           "#         Presione 'w' para moverse hacia arriba            #\n"
           "#         Presione 's' para moverse hacia abajo             #\n"
           "#      Presione 'e' para encender o apagar las luces        #\n"
           "#          Una luz prendida se representa con  '*'          #\n"
           "#          Una luz apagada se representa con   ' '          #\n"
           "#    El cursor en una luz apagada se representa con  '_'    #\n"
           "#    El cursor en una luz prendida se representa con '+'    #\n"
           "#                                                           #\n"
           "#            Presione 'q' para salir del juego              #\n"
           "#===========================================================#\n\n"
           );
}

void imprimirInstrucciones()
{
    printf("#===========================================================#\n"
           "#                INSTRUCCIONES DE JUEGO:                    #\n"
           "# El juego consiste en intentar apagar todas las luces que  #\n"
           "# Para ello hay hay que recorrer el tablero con las teclas  #\n"
           "#  'a', 'd', 'w', 's', y seleccionar una con la tecla 'e'   #\n"
           "#              para prender o apagar la luz.                #\n"
           "#  Cualquier luz cambiará su estado y el de las adyacentes  #\n"
           "#           (de apagado a encendido y viceversa).           #\n"
           "#                                                           #\n"
           "#            ¡MUCHA SUERTE EN EL JUEGO!                     #\n"
           "#===========================================================#\n"
           );
}

void imprimirTablero(int c)
{
    imprimirComandos();
    printf("                       ===========\n");

	for(int i = 0; i < TABLERO; i++)
	{

		if (i%FILA == 0)
			printf("                       |");
		if (i!= c)
			printf (" %c ", Casilleros.casillas[i]==0 ? ' ' : '*');
		else
		{
			printf (" %c ", Casilleros.casillas[i]==0 ? '_' : '+');
		}
        if (i%FILA == FILA -1)
			printf("|\n");
	}

	printf("                       ===========\n\n\n");

	imprimirInstrucciones();

}

void imprimirPerdedor()
{
    printf("  _______  _______  ______    ______   _______  ______   _______  ______     \n"
          " |       ||       ||    _ |  |      | |       ||      | |       ||    _ |    \n"
          " |    _  ||    ___||   | ||  |  _    ||    ___||  _    ||   _   ||   | ||    \n"
          " |   |_| ||   |___ |   |_||_ | | |   ||   |___ | | |   ||  | |  ||   |_||_   \n"
          " |    ___||    ___||    __  || |_|   ||    ___|| |_|   ||  |_|  ||    __  |  \n"
          " |   |    |   |___ |   |  | ||       ||   |___ |       ||       ||   |  | |  \n"
          " |___|    |_______||___|  |_||______| |_______||______| |_______||___|  |_|  \n\n\n"
          );
}

void imprimirGanador()
{
    printf("  _______  _______  __    _  _______  ______   _______  ______     \n"
          " |       ||   _   ||  |  | ||   _   ||      | |       ||    _ |    \n"
          " |    ___||  |_|  ||   |_| ||  |_|  ||  _    ||   _   ||   | ||    \n"
          " |   | __ |       ||       ||       || | |   ||  | |  ||   |_||_   \n"
          " |   ||  ||       ||  _    ||       || |_|   ||  |_|  ||    __  |  \n"
          " |   |_| ||   _   || | |   ||   _   ||       ||       ||   |  | |  \n"
          " |_______||__| |__||_|  |__||__| |__||______| |_______||___|  |_|  \n\n\n"
          );
}

int main()
{
	int cursor = 0;

    llenarTablero();

    lup:

    system("clear");
    imprimirTablero(cursor);

    int ch;
    static struct termios oldt, newt;

    tcgetattr( STDIN_FILENO, &oldt);
    newt = oldt;

    newt.c_lflag &= ~(ICANON);

    tcsetattr( STDIN_FILENO, TCSANOW, &newt);

    while((ch=getchar())!= 1)
    {
        if (ch == 'a' && cursor > 0)
        {
            cursor--;
            goto lup;
        }
        if (ch == 'd' && cursor < TABLERO -1)
        {
            cursor++;
            goto lup;
        }
        if (ch == 's' && cursor +FILA < TABLERO)
        {
            cursor += FILA;
            goto lup;
        }
        if (ch == 'w' && cursor -FILA > -1)
        {
            cursor -= FILA;
            goto lup;
        }
        if (ch == 'q')
        {
            tcsetattr( STDIN_FILENO, TCSANOW, &oldt);
            system("clear");
            imprimirPerdedor();
            exit(-1);
        }
        if (ch == 'e')
        {
            cambiarCasilla(cursor);
            if (tableroResuelto())
            {
                tcsetattr( STDIN_FILENO, TCSANOW, &oldt);
                system("clear");
                imprimirGanador();
                exit(-1);
            }
            else
            {
                goto lup;
            }
        }
        else
        {
            goto lup;
        }
    }

	return 0;
}
