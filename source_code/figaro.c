#include <stdio.h>      // para usar printf
#include <stdlib.h>     // para usar exit y funciones de la libreria standard
#include <pthread.h>    // para usar threads
#include <semaphore.h>  // para usar semaforos

#define NUM_THREADS 6
sem_t fiiig;
sem_t fig;
sem_t fi;
sem_t fa;
pthread_mutex_t mi_mutex;
int cont_figaro = 0;

void *fiiigaro()
{
    int i = 0;
    while(i < 10)
    {
        sem_wait(&fiiig);
        printf("Fiiiiigaro\n");
        sem_post(&fig);
        i++;
    }
}

void *figaro()
{
    int i = 0;
    while(i < 10)
    {
        sem_wait(&fig);
        pthread_mutex_lock(&mi_mutex);
        if(cont_figaro < 2)
        {
            cont_figaro++;
            printf("Figaro ");
            sem_post(&fig);
        }
        else
        {
            cont_figaro = 0;
            printf("Figaro\n");
            sem_post(&fi);
        }
        pthread_mutex_unlock(&mi_mutex);
        i++;
    }
}

void *figaro_fi()
{
    int i = 0;
    while(i < 10)
    {
        sem_wait(&fi);
        printf("Figaro Fi\n");
        sem_post(&fa);
        i++;
    }
}

void *figaro_fa()
{
    int i = 0;
    while(i < 10)
    {
        sem_wait(&fa);
        printf("Figaro Fa\n\n");
        sem_post(&fiiig);
        i++;
    }
}

int main()
{
    sem_init(&fiiig, 0, 1);
    sem_init(&fig, 0, 0);
    sem_init(&fi, 0, 0);
    sem_init(&fa, 0, 0);
    pthread_mutex_init(&mi_mutex, NULL);

    pthread_t figaros_t[NUM_THREADS];

    int rc;

    for(int i = 0; i < NUM_THREADS; i++)
    {
        if(i==0)
            rc = pthread_create(&figaros_t[0], NULL, fiiigaro, NULL);
        else if(i==4)
            rc = pthread_create(&figaros_t[4], NULL, figaro_fi, NULL);
        else if(i==5)
            rc = pthread_create(&figaros_t[5], NULL, figaro_fa, NULL);
        else
            rc = pthread_create(&figaros_t[i], NULL, figaro, NULL);

        if(rc)
        {
            printf("Error:unable to create thread, %d \n", rc);
            exit(-1);
        }
    }

    for(int i = 0; i < NUM_THREADS; i++)
        pthread_join(figaros_t[i], NULL);

    sem_destroy(&fiiig);
    sem_destroy(&fig);
    sem_destroy(&fi);
    sem_destroy(&fa);
    pthread_mutex_destroy(&mi_mutex);

    pthread_exit(NULL);
}
