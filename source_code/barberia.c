#include <stdio.h>    // para usar printf
#include <stdlib.h>         // para usar exit y funciones de la libreria standard
#include <pthread.h>    // para usar threads
#include <semaphore.h>

#define MAX_CLIENT 5
enum estados_del_barbero {durmiendo, cortando};
int cant_clientes = 0;
int estado_barber = durmiendo;
pthread_mutex_t mi_mutex;
sem_t barb;
sem_t client;
//sem_t aux;

void* barbero()
{
    int i = 0;
    while(i < MAX_CLIENT * 20)
    {
        sem_wait(&barb);
        //sem_wait(&aux);
        if (cant_clientes == 0)
        {
            pthread_mutex_lock(&mi_mutex);
            estado_barber = durmiendo;
            pthread_mutex_unlock(&mi_mutex);
            printf("ZZZZZ\thay %d clientes esperando.\n", cant_clientes);
        }
        else
        {
            pthread_mutex_lock(&mi_mutex);
            cant_clientes--;
            estado_barber = cortando;
            pthread_mutex_unlock(&mi_mutex);
            printf("XXXXX\thay %d clientes esperando.\n", cant_clientes);
        }
        sem_post(&client);
        //sem_post(&aux);
    i++;
    }
    pthread_exit(NULL);
}

void* cliente()
{
    int i = 0;
    while(i < 20)
    {
        sem_wait(&client);
        //sem_wait(&aux);
        if (cant_clientes < MAX_CLIENT)
        {
            pthread_mutex_lock(&mi_mutex);
            cant_clientes++;
            pthread_mutex_unlock(&mi_mutex);

            if (estado_barber == cortando)
            {
                printf("leyendo revista...\thay %d clientes esperando.\n", cant_clientes);
            }
            else if (estado_barber == durmiendo)
            {
                pthread_mutex_lock(&mi_mutex);
                estado_barber = cortando;
                pthread_mutex_unlock(&mi_mutex);
                printf("ejem, ejem..., cof, cof...\thay %d clientes esperando.\n", cant_clientes);
            }
        }
        else
        {
            printf("chau $ chau $...\thay %d clientes esperando.\n", cant_clientes);
        }
    sem_post(&barb);
    //sem_post(&aux);
    //sem_post(&client);
    i++;
    }
    pthread_exit(NULL);
}

int main ()
{
    sem_init(&barb, 0, 1);
    sem_init(&client, 0, 0);
    //sem_init(&aux, 0, 1);
    pthread_mutex_init (&mi_mutex, NULL);

    pthread_t barberia_t[MAX_CLIENT +1];
    int rc;

	for(int i = 0; i < MAX_CLIENT + 1; i++)
	{
        if(i == 0)
            rc = pthread_create(&barberia_t[i], NULL, barbero, NULL);
        else
            rc = pthread_create(&barberia_t[i], NULL, cliente, NULL);

        if (rc){
           printf("Error:unable to create thread, %d \n", rc);
           exit(-1);
        }
    }

    for(int i = 0; i < MAX_CLIENT +1; i++)
        pthread_join (barberia_t[i],NULL);

    pthread_exit(NULL);
    sem_destroy(&barb);
    sem_destroy(&client);
    //sem_destroy(&aux);
    pthread_mutex_destroy(&mi_mutex);

    return 0;
}
