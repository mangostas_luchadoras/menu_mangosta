#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
	red=`tput setaf 1`;
	green=`tput setaf 2`;
	bg_green=`tput setab 2`;
	blue=`tput setaf 4`;
	bg_blue=`tput setab 4`;
	reset=`tput sgr0`;
	bold=`tput setaf bold`;

#------------------------------------------------------
# TODO: Completar con su path
#------------------------------------------------------
proyectoActual="$HOME/git/menu_mangosta"

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "    __   __  _______  __    _  __   __    \n\t\t   |  |_|  ||       ||  |  | ||  | |  |   \n\t\t   |       ||    ___||   |_| ||  | |  |   \n\t\t   |       ||   |___ |       ||  |_|  |   \n\t\t   |       ||    ___||  _    ||       |   \n\t\t   | ||_|| ||   |___ | | |   ||       |   \n\t\t   |_|   |_||_______||_|  |__||_______|   \n  __   __  _______  __    _  _______  _______  _______  _______  _______  \n |  |_|  ||   _   ||  |  | ||       ||       ||       ||       ||   _   | \n |       ||  |_|  ||   |_| ||    ___||   _   ||  _____||_     _||  |_|  | \n |       ||       ||       ||   | __ |  | |  || |_____   |   |  |       | \n |       ||       ||  _    ||   ||  ||  |_|  ||_____  |  |   |  |       | \n | ||_|| ||   _   || | |   ||   |_| ||       | _____| |  |   |  |   _   | \n |_|   |_||__| |__||_|  |__||_______||_______||_______|  |___|  |__| |__| \n                                                                          ";

    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";

    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver estado del proyecto";
    echo -e "\t\t\t b.  Guardar cambios";
    echo -e "\t\t\t c.  Actualizar repo";
    echo -e "\t\t\t d.  Crear 100 carpetas";
    echo -e "\t\t\t e.  Eliminar 100 carpetas";
    echo -e "\t\t\t f.  Abrir en terminal";
    echo -e "\t\t\t g.  Abrir en carpeta";
    echo -e "\t\t\t h.  Light out - mangosta-game";
    echo -e "\t\t\t i.  Procesos y Fork";
    echo -e "\t\t\t j.  La Barberia";
    echo -e "\t\t\t k.  Figaro";
    echo -e "\t\t\t l.  Descarga axel";
    echo -e "\t\t\t m.  Descarga wget";
    echo -e "\t\t\t n.  Respuesta paralelismo y ejemplos de descargas";
    echo -e "\t\t\t o.  Abrir trello";

    echo -e "\t\t\t p.  Actualizar sistema e instalar utilidades";

    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t${bg_blue}${red}${bold}------------------------------------------\t${reset}";
    echo -e "\t\t${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t${bg_blue}${red}${bold}------------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
	echo $1;
	while true; do
		echo "desea ejecutar? (s/n)";
    		read respuesta;
    		case $respuesta in
        		[Nn]* ) break;;
       			[Ss]* ) eval $1
				break;;
        		* ) echo "Por favor tipear S/s ó N/n.";;
    		esac
	done
}



#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
    	imprimir_encabezado "\t Opción a.  Ver estado del proyecto ";
	echo "---------------------------"
	echo "Somthing to commit?"
        decidir "cd $proyectoActual; git status";

        echo "---------------------------"
	echo "Incoming changes (need a pull)?"
	decidir "cd $proyectoActual; git fetch origin"
	decidir "cd $proyectoActual; git log HEAD..origin/master --oneline"
}

b_funcion () {
       	imprimir_encabezado "\t Opción b.  Guardar cambios ";
       	decidir "cd $proyectoActual; git add -A";
       	echo "Ingrese mensaje para el commit:";
       	read mensaje;
       	decidir "cd $proyectoActual; git commit -m \"$mensaje\"";
       	decidir "cd $proyectoActual; git push";
}

c_funcion () {
      	imprimir_encabezado "\t Opción c.  Actualizar repo ";
      	decidir "cd $proyectoActual; git pull";
}

d_funcion () {
        imprimir_encabezado "\t Opción d.  Crear 100 carpetas ";
	mkdir $HOME/TPSOR

	for i in {0..99}
	do
		printf "\tCreando carpeta: %s\n" "numero_$i"

		mkdir $HOME/TPSOR/$i
		chmod 740 $HOME/TPSOR/$i
	done
}

e_funcion () {
        imprimir_encabezado "\t Opción e.  Borrar 100 carpetas ";
	echo Eliminando carpetas...
	rm -fr $HOME/TPSOR
	echo Carpetas eliminadas!
}

f_funcion () {
	imprimir_encabezado "\t Opción f.  Abrir en terminal ";
	decidir "cd $proyectoActual; xterm &";
}

g_funcion () {
	imprimir_encabezado "\t Opción g.  Abrir en carpeta ";
	decidir "gnome-open $proyectoActual &";
}

h_funcion () {
	imprimir_encabezado "\t Opción h.  Light out - mangosta-game ";
	xterm -e htop &
	decidir "$proyectoActual/programas/lightout";	
}

i_funcion () {
	imprimir_encabezado "\t Opción i.  Procesos y Fork ";
	decidir "clear; $proyectoActual/programas/fork_wait";
	esperar;
	clear;
	decidir "clear; cat $proyectoActual/recursos/respuesta_fork";
	esperar;
	clear;
	decidir "clear; less $proyectoActual/recursos/fork_arbol_procesos"
}

j_funcion () {
	imprimir_encabezado "\t Opción j.  La Barberia ";
	decidir "clear; $proyectoActual/programas/barberia";
	esperar;
	clear;
	decidir "clear; cat $proyectoActual/recursos/respuesta_condicion_carrera";
}

k_funcion () {
	imprimir_encabezado "\t Opción k.  Figaro ";
	decidir "clear; $proyectoActual/programas/figaro";
}

l_funcion () {
	imprimir_encabezado "\t Opción l.  Descarga axel ";
	echo "Descargar imagen iso de debian 10 utilisando axel"
	decidir "axel -n 10 -a http://debian.xfree.com.ar/debian-cd/10.0.0/amd64/iso-dvd/debian-10.0.0-amd64-DVD-1.iso";
}

m_funcion () {
	imprimir_encabezado "\t Opción m.  Descarga wget ";
	echo "Descargar imagen iso de debian 10 utilisando wget"
	decidir "wget http://debian.xfree.com.ar/debian-cd/10.0.0/amd64/iso-dvd/debian-10.0.0-amd64-DVD-1.iso";
}

n_funcion () {
	imprimir_encabezado "\t Opción n.  Respuesta paralelismo y ejemplos de descargas ";
	echo "Comparacion de timpo de descarga de axel y wget; y respuesta sobre paralelismo."
	decidir "clear; cat $proyectoActual/recursos/respuesta_paralelismo";
	esperar;
	clear;
	decidir "xloadimage $proyectoActual/recursos/axel.png $proyectoActual/recursos/wget.png"
}

o_funcion () {
	imprimir_encabezado "\t Opción o.  Abrir trello ";
	decidir "clear; firefox https://trello.com/b/f4leMmSf/tpsor1";
}

p_funcion () {
	imprimir_encabezado "\t Opción p.  Actualizar sistema e instalar utilidades ";
	decidir "clear; sudo apt update && sudo apt upgrade -y";
	decidir "sudo apt install xloadimage axel -y"
	decidir "sudo apt install firefox -y"
}

q_funcion () {
	clear;
	exit;
}

#------------------------------------------------------
# TODO: Completar con el resto de ejercicios del TP, una funcion por cada item
#------------------------------------------------------



#------------------------------------------------------
# Funciones Mangosta
#------------------------------------------------------

imprimir_presentacion () {

	clear;
	echo -e "${bold}${green}";
	cat $proyectoActual/recursos/presentacion;
	sleep 2s;
	echo -e "${reset}";
	clear;
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
imprimir_presentacion;

while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;

    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        f|F) f_funcion;;
        g|G) g_funcion;;
        h|H) h_funcion;;
        i|I) i_funcion;;
	j|J) j_funcion;;
	k|K) k_funcion;;
	l|L) l_funcion;;
	m|M) m_funcion;;
	n|N) n_funcion;;
        o|O) o_funcion;;
	p|P) p_funcion;;
        q|Q) q_funcion;;

        *) malaEleccion;;
    esac
    esperar;
done


